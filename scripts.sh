cd &&
apt-get update -y &&
apt-get install gcc -y &&
apt-get install unzip -y &&
apt-get install lua5.2 -y &&
apt-get install liblua5.2 -y &&
apt-get install liblua5.2-dev &&
wget https://keplerproject.github.io/luarocks/releases/luarocks-2.4.1.tar.gz &&
tar -xf luarocks-2.4.1.tar.gz &&
cd luarocks-2.4.1 &&
./configure --lua-version=5.2 --versioned-rocks-dir &&
make build &&
make install &&
luarocks-5.2 install net-url &&
luarocks-5.2 install basexx &&
apt-get install libssl-dev -y &&
luarocks-5.2 install luacrypto2 &&
mkdir src &&
cd src &&
luarocks-5.2 download lua-cjson &&
luarocks-5.2 unpack lua-cjson-2.1.0.6-1.src.rock &&
cd lua-cjson-2.1.0.6-1/lua-cjson &&
sed -i 's/lua_objlen/lua_rawlen/g' lua_cjson.c &&
sed -i 's|$(PREFIX)/include|/usr/include/lua5.2|g' Makefile &&
luarocks-5.2 make && 
cd &&
apt install git cmake liblua5.1-0-dev -y &&
git clone https://github.com/evanlabs/luacrypto &&
cd luacrypto &&
luarocks-5.2 make &&
cd &&
luarocks-5.2 install luajwtjitsi &&
cd &&
wget https://prosody.im/files/prosody-debian-packages.key -O- | sudo apt-key add - &&
echo deb http://packages.prosody.im/debian $(lsb_release -sc) main | sudo tee -a /etc/apt/sources.list &&
apt-get update -y &&
apt-get upgrade -y &&
apt-get install prosody -y &&
chown root:prosody /etc/prosody/certs/localhost.key &&
chmod 644 /etc/prosody/certs/localhost.key &&
sleep 2 &&
shutdown -r now